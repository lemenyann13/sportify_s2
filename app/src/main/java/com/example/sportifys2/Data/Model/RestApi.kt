package com.example.sportifys2.Data.Model

import com.example.sportifys2.Data.Model.LatLongModel
import retrofit2.Call
import retrofit2.http.*

interface RestApi {

    //@Headers("Content-Type: application/json")
    @POST("reperes")
    fun addRepere(@Body latlongData: LatLongModel) : Call<LatLongModel>
    @GET("reperes")
    fun getListeReperes(): Call<List<LatLongModel>>

}