package com.example.sportifys2.Data.Model

data class LatLongModel(
        var id : String? = null,
        var lat: String,
        var longi: String
)
