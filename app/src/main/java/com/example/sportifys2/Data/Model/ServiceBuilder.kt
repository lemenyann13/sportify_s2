package com.example.sportifys2.Data.Model

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


object ServiceBuilder {  // construction du retrofit

    private val url = "https://a957acb150b3.ngrok.io/api/"
    private val client = OkHttpClient.Builder().build()

    val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(MoshiConverterFactory.create()).build()

    fun buildService(): RestApi {
        return retrofit.create(RestApi::class.java)
    }
}