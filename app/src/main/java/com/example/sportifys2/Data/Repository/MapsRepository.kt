package com.example.sportifys2.Data.Repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.sportifys2.Data.Model.LatLongModel
import com.example.sportifys2.Data.Model.RestApi
import com.example.sportifys2.Data.Model.ServiceBuilder
import retrofit2.Call;
import retrofit2.Callback
import retrofit2.Response


class MapsRepository {
    private var restApi: RestApi? = null
    private var listeReperes: MutableLiveData<List<LatLongModel>>

    /*private var newsRepository: MapsRepository? = null

    fun getInstance(): MapsRepository? {  // singleton , a modifier pour faire une version "kotlin" avec object
        if (newsRepository == null) {
            newsRepository = MapsRepository()
        }
        return newsRepository
    }*/

    init{
        restApi = ServiceBuilder.buildService()
        listeReperes = MutableLiveData()
    }

    fun ajouteMark(latLongData: LatLongModel) {

        println("lat  ${latLongData.lat}  long ${latLongData.longi}")
        restApi?.addRepere(latLongData)?.enqueue(object : Callback<LatLongModel> { // lambda qui définit la méthode se trouvant dans l'interface RestApi, c'est ici que l'app communique avec symfony
            override fun onResponse(
                call: Call<LatLongModel>,
                response: Response<LatLongModel>
            ) {

                val addedRepere = response.body()
                println("test response post ${addedRepere.toString()}")

                Log.i("POST_SUCCESS", "Post devops ${addedRepere?.lat} et ${addedRepere?.longi}")
                if(response.isSuccessful)
                    println("success add post")
                Result.success(addedRepere)
                addedRepere.let {
                    if (it != null) {
                        Log.i("POST_SUCCESS", "Post devops ${it.lat} et ${it.longi}")
                    }
                }
            }

            override fun onFailure(call: Call<LatLongModel>, t: Throwable) {
                Log.e("TYPE", "Erreur api devops : $t")
            }
        })
    }

    /**
     * A déplacer dans le fragment map qui affiche la liste
     */
    fun getMarks(): MutableLiveData<List<LatLongModel>> {

        println("getmarks enter")
        println(restApi?.getListeReperes()?.isExecuted)
        restApi?.getListeReperes()?.enqueue(object : Callback<List<LatLongModel>> { // lambda qui définit la méthode se trouvant dans l'interface RestApi, c'est ici que l'app communique avec symfony
            override fun onResponse(
                call: Call<List<LatLongModel>>,
                response: Response<List<LatLongModel>>
            ) {

                val getRepere = response.body()
                println("test response get ${getRepere.toString()}")
                listeReperes.value = getRepere

                getRepere.let {
                    if( it != null ){
                        for(repere in it){
                            Log.i("GET_SUCCESS","Get devops latitude ${repere.longi} et ${repere.longi}" )
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<LatLongModel>>, t: Throwable) {
                println("erreur api")
                Log.e("TYPE", "Erreur api devops : $t")
            }
        })
        println("test return ${listeReperes.value}")
        return listeReperes
    }
}