package com.example.sportifys2.UI.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sportifys2.Data.Model.LatLongModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline

class CoursesListeViewModel : ViewModel() {

    private lateinit var polyline: Polyline
    var coordonnees = MutableLiveData<MutableList<LatLongModel>>()  // Utilisation de LiveData en tant que data holder
    private var listeReperes: MutableLiveData<List<LatLongModel>>
    private val apiKey: String = "AIzaSyCrjvL44pzOyaGAsKpyH9jNsYX8kUO6cHk"

    init{
        Log.i("CoursesListeViewModel", "CoursesListeViewModel created")
        coordonnees.value = ArrayList()
        listeReperes = MutableLiveData()
    }

    fun getUrl(origin: LatLng, destination: LatLng): String {       // on construit l'url qui donne la trajectoire
        var str_origin: String = "origin="+origin.latitude+","+origin.longitude
        var str_destination: String = "destination="+destination.latitude+","+destination.longitude
        var output = "json";
        var url = "https://maps.googleapis.com/maps/api/directions/$output?&mode=walking&$str_origin&$str_destination&key=$apiKey"
        return url
    }
}