package com.example.sportifys2.UI.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sportifys2.Data.Model.LatLongModel
import com.example.sportifys2.Data.Repository.MapsRepository
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline


class MapsViewModel : ViewModel(){

    private lateinit var polyline: Polyline
    var coordonnees = MutableLiveData<MutableList<LatLongModel>>()  // Utilisation de LiveData en tant que data holder
    private var listeReperes: MutableLiveData<List<LatLongModel>>
    private val apiKey: String = "AIzaSyCrjvL44pzOyaGAsKpyH9jNsYX8kUO6cHk"

    init{
        Log.i("MapsViewModel", "MapsViewModel created")
        coordonnees.value = ArrayList()
        listeReperes = MutableLiveData()
    }

    fun addLatLong(pLat: Double, pLong: Double){
        val apiRest = MapsRepository()
        val latlong = LatLongModel(  // on fait une instanciation de la data class LatLongModel qui représente les données du repère (latitude, longitude)
            lat = pLat.toString(),
            longi = pLong.toString()
        )
        apiRest.ajouteMark(latlong) // on appel la méthode du repository qui s'occupe du POST
        /*val model = LatLongModel(pLat, pLong)
        coordonnees.value?.add(model)*/
    }

    fun getLatLong(): List<LatLongModel>? {
        val apiRest = MapsRepository()
        listeReperes = apiRest.getMarks()
        println(" viewmodel ${listeReperes.value}")
        return listeReperes.value
    }
}