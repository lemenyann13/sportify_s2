package com.example.sportifys2.UI.View

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.sportifys2.Data.Model.LatLongModel
import com.example.sportifys2.UI.ViewModel.MapsViewModel
import com.example.sportifys2.R
import com.example.sportifys2.databinding.MapsFragmentBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsFragment : Fragment()  {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var viewModel: MapsViewModel
    private lateinit var binding: MapsFragmentBinding
    private var pointsList: MutableList<LatLng> = ArrayList() // Liste des points mis sur la map par l'utilisateur (on veut 2 max pour le trajet)

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    private val callback = OnMapReadyCallback { googleMap ->   // lorsque la map est chargée, les instructions suivantes exécutées
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */

        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true
        //val currentLocation = LatLng(48.89257049560547, 2.235877513885498)
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 12f))
        //map.addMarker(MarkerOptions().position(currentLocation).title("Marker example"))

        map.setOnMapClickListener {             // Lorsqu'on clique sur la map, on place un repère
                p0 ->
            val location = LatLng(p0.latitude, p0.longitude)
            if(pointsList.size < 2) {
                pointsList.add(location)
                map.addMarker(MarkerOptions().position(p0)) // ajout du repère sur la carte
                Log.i("addMarker", "point ajouté")
            }
        }
        map.setOnMarkerClickListener {      // Lorsqu'on clique sur un repère,  on le supprime de la carte
                p0 ->
            p0?.remove()
            pointsList.remove(pointsList.last())  // remove du repère
            Log.i("deleteMarker", "point retiré")
            return@setOnMarkerClickListener true;
        }
        binding.btnTrajet.setOnClickListener {      // lorsqu'on clique sur le bouton, on veut alimenter la base de données
            if (pointsList.size.equals(2)) {
                viewModel.addLatLong(pointsList[0].latitude, pointsList[0].longitude) // appel méthode viewmodel qui s'occupe  de transmettre la data
                viewModel.addLatLong(pointsList[1].latitude, pointsList[1].longitude)
                view?.findNavController()?.navigate(R.id.action_mapsFragment_to_formulaireFragment)
                map.clear()
            }

            println("Coordonnées bien ajoutées ${viewModel.coordonnees.value}")
        }
        setUpMap()
        //afficheGetMarks()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.maps_fragment, container, false)
        Log.i("MapsFragment", "Called Viewmodel")
        viewModel = ViewModelProviders.of(this).get(MapsViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        setHasOptionsMenu(true)
        return binding.root;
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.mapsFragment) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    /**
     * Permet d'accéder et de récupérer la localisation
     */
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this.requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        map.isMyLocationEnabled = true

// 2
        fusedLocationClient.lastLocation.addOnSuccessListener(this.requireActivity()) { location ->
            // Got last known location. In some rare situations this can be null.
            // 3
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    private fun afficheGetMarks(){
         for(point: LatLongModel in viewModel.getLatLong()!!){
             val latlng = LatLng(point.lat.toDouble(), point.longi.toDouble())
             Log.i("Affiche points ", "latitude : ${latlng.latitude} et longitude : ${latlng.longitude}")
             map.addMarker(MarkerOptions().position(latlng))
         }
     }
}
