package com.example.sportifys2.UI.View

import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.sportifys2.Data.Model.LatLongModel
import com.example.sportifys2.UI.ViewModel.MapsViewModel
import com.example.sportifys2.R
import com.example.sportifys2.UI.ViewModel.CoursesListeViewModel
import com.example.sportifys2.databinding.FragmentCoursesListeBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import org.json.JSONObject

class CoursesListeFragment : Fragment()  {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var viewModel: CoursesListeViewModel
    private lateinit var binding: FragmentCoursesListeBinding
    private var pointsList: MutableList<LatLng> = ArrayList() // Liste des points mis sur la map par l'utilisateur (on veut 2 max pour le trajet)

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private val callback = OnMapReadyCallback { googleMap ->   // lorsque la map est chargée, les instructions suivantes exécutées
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        println("TEST MAP ACTION")
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true

        val location1 = LatLng(48.90729327843738,2.2352126286954244)
        val location2 = LatLng(48.88947447335264,2.2403568152195374)
        pointsList.add(location1)
        pointsList.add(location2)


        traceTrajets()
        setUpMap()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_courses_liste, container, false)
        Log.i("FramentCourseListe", "Called Viewmodel")
        viewModel = ViewModelProviders.of(this).get(CoursesListeViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        setHasOptionsMenu(true)
        return binding.root;
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("onViewCreated")
        val mapFragment = childFragmentManager.findFragmentById(R.id.coursesListeFragment) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    /**
     * Permet d'accéder et de récupérer la localisation
     */
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this.requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        map.isMyLocationEnabled = true

// 2
        fusedLocationClient.lastLocation.addOnSuccessListener(this.requireActivity()) { location ->
            // Got last known location. In some rare situations this can be null.
            // 3
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    private fun traceTrajets(){
        val path: MutableList<List<LatLng>> = ArrayList()
        //val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?&mode=walking&origin=${pointsList.get(0).latitude},${pointsList.get(0).longitude}&destination=${pointsList.get(1).latitude},${pointsList.get(1).longitude}&key=AIzaSyA_XzKVgSEGTyDmqMXcy11DHKKIod3Y4-o"
        val urlDirections = viewModel.getUrl(pointsList[0], pointsList[1])
        Log.i("testURL", urlDirections)
        println(urlDirections)

        val directionsRequest = object : StringRequest(
            Request.Method.GET,
            urlDirections,
            Response.Listener<String> { response ->
                val jsonResponse = JSONObject(response)
                // Get routes
                val routes = jsonResponse.getJSONArray("routes")
                val legs = routes.getJSONObject(0).getJSONArray("legs")
                val steps = legs.getJSONObject(0).getJSONArray("steps")
                for (i in 0 until steps.length()) {
                    val points =
                        steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                    path.add(PolyUtil.decode(points))
                }
                for (i in 0 until path.size) {

                    map.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                }
            },
            Response.ErrorListener { _ ->
            }){}
        val requestQueue = Volley.newRequestQueue(this.requireActivity())
        requestQueue.add(directionsRequest)
    }
}
